export const Pelatih = {
    data(){
        return {
            coachs: [
                {
                    id : 1,
                    nama: 'Jose Mourinho'
                },
                {
                    id : 2,
                    nama: 'Alex Ferguson'
                },
                {
                    id : 3,
                    nama: 'Josep Guardiola'
                },
                {
                    id : 4,
                    nama: 'Arsene Wenger'
                },
            ]
        }
    },
    template : `
    <div>
        <h1>Daftar Pelatih  Bola</h1>
        <ul>
            <li v-for="coach of coachs">
            <router-link :to="'/coach/'+coach.id">
            {{ coach.nama }}
            </router-link>
            </li>
        </ul>
    </div>
    `
}