export const Kiper = {
    data(){
        return {
            kipers: [
                {
                    id : 1,
                    nama: 'Alisson Becker'
                },
                {
                    id : 2,
                    nama: 'Jan Oblak '
                },
                {
                    id : 3,
                    nama: 'Ederson Moraes'
                },
                {
                    id : 4,
                    nama: 'Emiliano Martinez'
                },
            ]
        }
    },
    template : `
    <div>
        <h1>Daftar Kiper Bola</h1>
        <ul>
            <li v-for="kiper of kipers">
            <router-link :to="'/kiper/'+kiper.id">
            {{ kiper.nama }}
            </router-link>
            </li>
        </ul>
    </div>
    `
}