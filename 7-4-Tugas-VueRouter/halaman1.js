export const StrikerBola = {
    data(){
        return {
            strikers: [
                {
                    id : 1,
                    nama: 'Lionel Messi'
                },
                {
                    id : 2,
                    nama: 'Robert Lewandowski'
                },
                {
                    id : 3,
                    nama: 'Karim Benzema'
                },
                {
                    id : 4,
                    nama: 'Cristiano Ronaldo'
                },
            ]
        }
    },
    template : `
    <div>
        <h1>Daftar Striker Bola</h1>
        <ul>
            <li v-for="striker of strikers">
            <router-link :to="'/striker/'+striker.id">
                {{ striker.nama }}
            </router-link>
            </li>
        </ul>
    </div>
    `
}