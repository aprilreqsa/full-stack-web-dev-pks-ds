<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Comment;
use App\Events\CommentStoredEvent;
use App\Mail\CommentAuthorMail;
use App\Mail\PostAuthorMail;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{
    public function index()
    {
        $comment = Comment::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data' => $comment
        ],200);
    }
    public function show($id)
    {
        $comment = Comment::FindOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data' => $comment
        ],200);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);
        event(new CommentStoredEvent($comment));
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        if ($comment){
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data' => $comment
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment failed to Save'
        ],409);
    }
    public function update(Request $request, Comment $comment)
    {
        $validator = Validator::make($request->all(),[
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        $comment = Comment::FindOrFail($comment->id);
        if ($comment){
            $user = auth()->user();
            if ($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik user login',
                ],403);
            }
            $comment->update([
                'content' => $request->content,
                'post_id' => $request->post_id
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data' => $comment
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment not found',
        ],404);
    }
    public function destroy($id)
    {
        $comment = Comment::FindOrFail($id);
        if ($comment){
            $user = auth()->user();
            if ($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik user login',
                ],403);
            }
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment not found',
        ],404);
    }
}
