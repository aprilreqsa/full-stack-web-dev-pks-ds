<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $post
        ],200);
    }
    public function show($id)
    {
        $post = Post::FindOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data' => $post
        ],200);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        // $user = auth()->user();

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            // 'user_id' => $user->id
        ]);
        if ($post){
            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data' => $post
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post failed to Save'
        ],409);
    }
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        $post = Post::FindOrFail($post->id);
        
        if ($post){
            $user = auth()->user();
            if ($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user login',
                ],403);
            }
            $post->update([
                'title' => $request->title,
                'description' => $request->description
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data' => $post
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post not found',
        ],404);
    }
    public function destroy($id)
    {
        $post = Post::FindOrFail($id);
        if ($post){
            $user = auth()->user();
            if ($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user login',
                ],403);
            }
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post not found',
        ],404);
    }
}
