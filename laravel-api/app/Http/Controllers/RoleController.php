<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Role',
            'data' => $role
        ],200);
    }
    public function show($id)
    {
        $role = Role::FindOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data' => $role
        ],200);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $role = Role::create([
            'name' => $request->name,
        ]);
        if ($role){
            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data' => $role
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role failed to Save'
        ],409);
    }
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        $role = Role::FindOrFail($role->id);
        if ($role){
            $role->update([
                'name' => $request->name,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data' => $role
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role not found',
        ],404);
    }
    public function destroy($id)
    {
        $role = Role::FindOrFail($id);
        if ($role){
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role not found',
        ],404);
    }
}
