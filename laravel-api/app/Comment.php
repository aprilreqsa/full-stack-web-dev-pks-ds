<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;
class comment extends Model
{
    protected $table = "comments";
    protected $fillable = ['content','post_id','user_id'];
    protected $keyType = 'string';
    public $incrementing = false;
    public function post(){
        return $this->belongsTo('App\Post');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
